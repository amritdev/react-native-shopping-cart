import firestore from '@react-native-firebase/firestore';
import useCollection from 'src/hooks/useCollection';
import {
  CartItem,
  getProductCartItems,
  getTotalPrice,
  ProductCartItem,
} from 'src/models/cart-item';
import {Product} from 'src/models/product';

const cartItemsCollection = firestore().collection('cart-items');

const useCart = (products: Product[]) => {
  const {loading, items, error} = useCollection<CartItem>(cartItemsCollection);

  const productCartItems = getProductCartItems(products, items || []);

  const setQuantity = async (cartItemId: string, quantity: number) => {
    if (quantity === 0) {
      await cartItemsCollection.doc(cartItemId).delete();
      return;
    }
    const documentSnapshot = await cartItemsCollection.doc(cartItemId).get();
    if (!documentSnapshot.exists) {
      console.warn(`cart item ${cartItemId} not exists`);
      return;
    }
    documentSnapshot.ref.update({count: quantity});
  };

  const addToCart = async (product: Product) => {
    let productCartItem: Partial<ProductCartItem> | undefined =
      productCartItems.find(
        ({product_id: productId}) => productId === product.id,
      );
    if (!productCartItem) {
      productCartItem = {
        product_id: product.id,
        count: 1,
      };
      await cartItemsCollection.add(productCartItem);
      return;
    }
    await setQuantity(productCartItem.id!, productCartItem.count! + 1);
  };

  return {
    loading,
    items,
    productCartItems,
    totalPrice: getTotalPrice(productCartItems),
    error,
    setQuantity,
    addToCart,
  };
};

export default useCart;
