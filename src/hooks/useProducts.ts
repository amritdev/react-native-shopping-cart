import firestore from '@react-native-firebase/firestore';
import useCollection from 'src/hooks/useCollection';
import {Product} from 'src/models/product';

const productsCollection = firestore().collection('products');

const useProducts = () => {
  const {
    loading,
    items: products,
    error,
  } = useCollection<Product>(productsCollection);

  return {
    loading,
    products,
    error,
  };
};

export default useProducts;
