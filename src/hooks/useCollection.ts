import {FirebaseFirestoreTypes} from '@react-native-firebase/firestore';
import _ from 'lodash';
import {useEffect, useState} from 'react';

function useCollection<T>(
  collection: FirebaseFirestoreTypes.CollectionReference<FirebaseFirestoreTypes.DocumentData>,
  converter: (
    documentSnapshot: FirebaseFirestoreTypes.QueryDocumentSnapshot<FirebaseFirestoreTypes.DocumentData>,
  ) => T = documentSnapshot => {
    const id = documentSnapshot.id;
    const data = documentSnapshot.data();
    return {id, ...data} as any as T;
  },
) {
  const [items, setItems] = useState<T[]>();
  const [error, setError] = useState<Error>();

  useEffect(() => {
    const subscribe = collection.onSnapshot(querySnapshot => {
      const newItems: T[] = [];
      querySnapshot.forEach(documentSnapshot => {
        newItems.push(converter(documentSnapshot));
      });
      setItems(oldItems => {
        if (!_.isEqual(oldItems, newItems)) {
          return newItems;
        }
        return oldItems;
      });
    }, setError);

    return () => subscribe();
  }, [collection, converter]);

  return {
    loading: items === undefined,
    items,
    error,
  };
}

export default useCollection;
