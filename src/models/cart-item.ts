import {Product} from 'src/models/product';

export type CartItem = {
  id: string;
  product_id: string;
  count: number;
};

export type ProductCartItem = CartItem & {product: Product};

export function getProductCartItems(
  products: Product[],
  cartItems: CartItem[],
) {
  return cartItems
    .map(item => {
      const product = products.find(({id}) => {
        return id === item.product_id;
      });
      return {
        ...item,
        product,
      };
    })
    .filter(({product}) => !!product) as ProductCartItem[];
}

export function getTotalPrice(cartItems: ProductCartItem[]) {
  let total = 0;
  cartItems.forEach(item => {
    total += item.count * item.product.price;
  });

  return total;
}
