import React from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import {LinearProgress, Text} from 'react-native-elements';
import {Card} from 'react-native-elements';
import {Button} from 'react-native-elements';
import {Product} from 'src/models/product';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    width: '100%',
  },
  priceLabel: {
    width: '100%',
    textAlign: 'right',
    fontWeight: '700',
    marginBottom: 10,
  },
});

const ProductItem = ({
  item,
  onAddToCart,
}: {
  item: Product;
  onAddToCart: () => void;
}) => {
  return (
    <Card>
      <Card.Title>{item.name}</Card.Title>
      <Card.Divider />
      <Card.Image source={{uri: item.image}} />
      <Card.Divider />
      <Text style={styles.priceLabel}>Price: {item.price} $</Text>
      <Button onPress={onAddToCart} title="Add To Cart" />
    </Card>
  );
};

const Products = ({
  loading,
  products,
  addToCart,
}: {
  loading: boolean;
  products: Product[] | undefined;
  addToCart: (item: Product) => void;
}) => {
  return (
    <View style={styles.container}>
      {loading && <LinearProgress color="primary" />}
      <FlatList
        data={products || []}
        keyExtractor={item => item.id}
        renderItem={({item}) => (
          <ProductItem item={item} onAddToCart={() => addToCart(item)} />
        )}
        ListEmptyComponent={<Text>No products</Text>}
      />
    </View>
  );
};

export default Products;
