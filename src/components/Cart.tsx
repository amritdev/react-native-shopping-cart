import React from 'react';
import {FlatList, StyleSheet, TouchableOpacity, View} from 'react-native';
import {ListItem, LinearProgress, Avatar, Text} from 'react-native-elements';
import {ProductCartItem} from 'src/models/cart-item';

const styles = StyleSheet.create({
  subtitle: {},
  subtitleWrapper: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  quantity: {
    fontSize: 16,
    fontWeight: '700',
    width: 30,
    textAlign: 'center',
  },
  control: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: 20,
    height: 20,
    textAlign: 'center',
    borderRadius: 10,
    borderColor: 'blue',
    borderWidth: 1,
  },
  total: {
    marginTop: 20,
    textAlign: 'right',
    fontWeight: '700',
    paddingRight: 10,
    fontSize: 18,
  },
});

const CartItem = ({
  item,
  setQuantity,
}: {
  item: ProductCartItem;
  setQuantity: (quantity: number) => void;
}) => (
  <ListItem bottomDivider>
    <Avatar source={{uri: item.product.image}} />
    <ListItem.Content>
      <ListItem.Title>
        <Text>{item.product.name}</Text>
      </ListItem.Title>
      <ListItem.Subtitle style={styles.subtitle}>
        <View style={styles.subtitleWrapper}>
          <TouchableOpacity
            style={styles.control}
            onPress={() => setQuantity(item.count - 1)}>
            <Text>-</Text>
          </TouchableOpacity>
          <Text style={styles.quantity}>{item.count}</Text>
          <TouchableOpacity
            style={styles.control}
            onPress={() => setQuantity(item.count + 1)}>
            <Text>+</Text>
          </TouchableOpacity>
        </View>
      </ListItem.Subtitle>
    </ListItem.Content>
  </ListItem>
);

const Cart = ({
  items,
  setQuantity,
  loading,
  totalPrice,
}: {
  loading: boolean;
  items: ProductCartItem[];
  setQuantity: (item: ProductCartItem, quantity: number) => void;
  totalPrice: number;
}) => {
  return (
    <View>
      {loading && <LinearProgress color="primary" />}
      <FlatList
        data={items}
        keyExtractor={item => item.id}
        renderItem={({item}) => (
          <CartItem
            item={item}
            setQuantity={quantity => setQuantity(item, quantity)}
          />
        )}
      />
      <Text style={styles.total}>Total {totalPrice}$</Text>
    </View>
  );
};

export default Cart;
