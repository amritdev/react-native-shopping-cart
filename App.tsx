import React, {useState} from 'react';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  useColorScheme,
  View,
} from 'react-native';
import {Tab, TabView} from 'react-native-elements';

import {Colors} from 'react-native/Libraries/NewAppScreen';
import Cart from 'src/components/Cart';
import Products from 'src/components/Products';
import useCart from 'src/hooks/useCart';
import useProducts from 'src/hooks/useProducts';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  tabView: {
    width: '100%',
  },
});

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  const [currentTab, setCurrentTab] = useState(0);

  const {loading, products} = useProducts();
  const {
    productCartItems,
    loading: cartLoading,
    addToCart,
    setQuantity,
    totalPrice,
  } = useCart(products || []);

  return (
    <SafeAreaView style={[backgroundStyle, styles.container]}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <View style={styles.container}>
        <Tab value={currentTab} onChange={setCurrentTab}>
          <Tab.Item title="Product" />
          <Tab.Item title="Cart" />
        </Tab>
        <TabView value={currentTab} onChange={setCurrentTab}>
          <TabView.Item style={styles.tabView}>
            <Products
              products={products}
              loading={loading}
              addToCart={addToCart}
            />
          </TabView.Item>
          <TabView.Item style={styles.tabView}>
            <Cart
              items={productCartItems}
              loading={loading || cartLoading}
              setQuantity={(item, quantity) => setQuantity(item.id, quantity)}
              totalPrice={totalPrice}
            />
          </TabView.Item>
        </TabView>
      </View>
    </SafeAreaView>
  );
};

export default App;
